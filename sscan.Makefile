where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile




APP:=sscanApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

USR_INCLUDES += -I$(where_am_I)$(APPSRC)

USR_CFLAGS   += -Wno-unused-variable
USR_CFLAGS   += -Wno-unused-function
USR_CFLAGS   += -Wno-unused-but-set-variable
USR_CPPFLAGS += -Wno-unused-variable
USR_CPPFLAGS += -Wno-unused-function
USR_CPPFLAGS += -Wno-unused-but-set-variable

USR_CPPFLAGS += -DUSE_TYPED_RSET
# menuSscan doesn't have source file
DBDINC_SRCS += $(APPSRC)/scanparmRecord.c
DBDINC_SRCS += $(APPSRC)/sscanRecord.c

DBDINC_DBDS = $(subst .c,.dbd,   $(DBDINC_SRCS:$(APPSRC)/%=%))
DBDINC_HDRS = $(subst .c,.h,     $(DBDINC_SRCS:$(APPSRC)/%=%))
DBDINC_DEPS = $(subst .c,$(DEP), $(DBDINC_SRCS:$(APPSRC)/%=%))

HEADERS += $(APPSRC)/recDynLink.h
HEADERS += $(DBDINC_HDRS)
HEADERS += menuSscan.h

SOURCES += $(APPSRC)/recDynLink.c
SOURCES += $(APPSRC)/req_file.c

# SNCSEQ is always ON
SOURCES += $(APPSRC)/scanProg.st
SOURCES += $(APPSRC)/saveData_writeXDR.c
SOURCES += $(APPSRC)/writeXDR.c
# DBDINC_SRCS should be last of the series of SOURCES
SOURCES += $(DBDINC_SRCS)


DBDS += $(APPSRC)/sscanProgressSupport.dbd
DBDS += $(APPSRC)/sscanSupport.dbd

TEMPLATES += $(wildcard $(APPDB)/*.db)
#TEMPLATES += $(wildcard $(APPDB)/*.template)
#TEMPLATES += $(wildcard $(APPDB)/*.substitutions)

$(DBDINC_DEPS): $(DBDINC_HDRS) menuSscan.h

.dbd.h: 
	$(DBTORECORDTYPEH)  $(USR_DBDFLAGS) -o $@ $<



vpath %.dbd $(where_am_I)$(APPSRC)

menuSscan.h: menuSscan.dbd
	$(DBTOMENUH) $(USR_DBDFLAGS) -o $@ $<


.PHONY: vlibs
vlibs:
